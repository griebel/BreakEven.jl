# BreakEven.jl

## Usage

### Install with HTTPS

```julia-repl
julia> add https://codeberg.org/griebel/BreakEven.jl.git
julia> using BreakEven
```

### Install with SSH

```julia-repl
julia> add git@codeberg.org:griebel/BreakEven.jl.git
julia> using BreakEven
```

### Example

Define a `Position` with 

```julia-repl
julia> weight = 0.112
julia> price_buy = 710
julia> fee_buy = 0.5 # in percent
julia> fee_sell = 0.5 # in percent
julia> pos = Position(weight, price_buy, fee_buy, fee_sell)
```

Show the gain at a certain value

```julia-repl
julia> price_sell = 730
julia> gain(pos, price_sell)
```

Plot a `Position` on a graph. Plot from `price_buy` to a `factor` of this. 

```julia-repl
julia> factor = 1.1
julia> breakeven(pos, factor)
```

## Development

```julia-repl
julia> ] develop git@codeberg.org:griebel/BreakEven.jl.git
julia> ; cd ~/.julia/dev/BreakEven
julia> using Revise
julia> using BreakEven
```

**Side note on how to create a project from scratch**

1. Generate a new project with the name *BreakEven*
    ```julia-repl
    julia> ] generate BreakEven
    ```
2. Create a new git repository and dd the project
3. check out and use as described under development
4. To add a package dependency
    ```julia-repl
    julia> ; cd ~/.julia/dev/BreakEven
    julia> ] activate .
    julia> ] add Plots
    ```
