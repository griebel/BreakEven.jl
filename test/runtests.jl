using BreakEven
using Test

# Do tests work at all?
@test 1 == 1

# percent function
@test BreakEven.percent(5.0) == 0.05
@test BreakEven.percent(0.5) == 0.005

# round to correct value with 2 digits
@test BreakEven.currency_f(2.333) == 2.33