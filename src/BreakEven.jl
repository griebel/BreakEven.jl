module BreakEven

# How to setup a new Module
# julia> ] generate BreakEven
# julia> develop git@codeberg.org:griebel/BreakEven.jl.git
# cd ~/.julia/dev/BreakEven
# julia> ] activate .
# julia> ] add Plots

export Position
export breakeven
export gain

using Plots

@doc raw"""
    Position

Structure for a position

```julia-repl
julia> p = Position(0.112, 710, 0.5, 0.5)
```
"""
mutable struct Position
    weight::AbstractFloat
    price_buy::Int64
    fee_buy::AbstractFloat
    fee_sell::AbstractFloat
end

function percent(x::AbstractFloat)
    x / 100
end

function currency_f(x::AbstractFloat)
    floor(x, digits=2)
end

function currency_c(x::AbstractFloat)
    ceil(x, digits=2)
end


@doc raw"""
    gain(p::Position, x::AbstractFloat)

Value gain at a specified price

```julia-repl
julia> p = Position(0.112, 710, 0.5, 0.5)
julia> gain(p, 730.0)
```
"""
function gain(p::Position, x::AbstractFloat)
    # Revenue for selling
    revenue_sell = currency_f(p.weight * x)

    # Actual buy price
    value_buy = currency_c(p.weight * p.price_buy)

    # Fees for buying
    fees_buy = currency_c(revenue_sell * percent(p.fee_sell))

    # Fees for selling
    fees_sell = currency_c(value_buy * percent(p.fee_buy))

    # Total buy price
    buy = value_buy + fees_sell + fees_buy

    # Gained money after fees
    currency_c(revenue_sell - buy)
end


function breakeven(p::Position, target::AbstractFloat)
    win(x) = gain(p, x)
    plot(win, Int(floor(p.price_buy)), Int(ceil(target * p.price_buy)))
end

end # module
